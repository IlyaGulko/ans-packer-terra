variable "vpc_cidr" {
  default = ""
}

// ============================================================
//   Variable structure:
// [ {       ...        },
//   { az      = STRING    <-- availability zone
//     public  = STRING    <-- IP for public subnet in this AZ
//     private = STRING }, <-- IP for private subnet in this AZ
//   {       ...        } ]

variable "azs_n_cidrs" {
  type = "list"
  default = []
}

// ============================================================
// "Parsing" main list to divide all items to different lists

data "template_file" "public_cidrs" {
  count = "${length(var.azs_n_cidrs)}"
  template = "${lookup(var.azs_n_cidrs[count.index], "public")}"
}
data "template_file" "private_cidrs" {
  count = "${length(var.azs_n_cidrs)}"
  template = "${lookup(var.azs_n_cidrs[count.index], "private")}"
}
data "template_file" "azs" {
  count = "${length(var.azs_n_cidrs)}"
  template = "${lookup(var.azs_n_cidrs[count.index], "az")}"
}

// ============================================================
// Making local vars for easier usage

locals {
  vpc_id        = "${aws_vpc.DEMO.id}"
  public_cidrs  = "${data.template_file.public_cidrs.*.rendered}"
  private_cidrs = "${data.template_file.private_cidrs.*.rendered}"
  azs           = "${data.template_file.azs.*.rendered}"
}

// ============================================================