resource "aws_subnet" "public_subnets" {
  count                   = "${length(var.azs_n_cidrs)}"
  depends_on              = ["aws_vpc.DEMO"]
  availability_zone       = "${local.azs[count.index]}"
  cidr_block              = "${local.public_cidrs[count.index]}"
  vpc_id                  = "${local.vpc_id}"
  map_public_ip_on_launch = true
  tags {
    Name = "public_subnet_${count.index}-${terraform.workspace}"
  }
}
resource "aws_subnet" "private_subnets" {
  count             = "${length(var.azs_n_cidrs)}"
  depends_on        = ["aws_vpc.DEMO"]
  availability_zone = "${local.azs[count.index]}"
  cidr_block        = "${local.private_cidrs[count.index]}"
  vpc_id            = "${local.vpc_id}"
  tags {
    Name = "private_subnet_${count.index}-${terraform.workspace}"
  }
}