output "vpc_id" {
  value = "${local.vpc_id}"
}
output "vpc_cidr" {
  value = "${aws_vpc.DEMO.cidr_block}"
}
output "public_cidrs" {
  value = "${local.public_cidrs}"
}
output "private_cidrs" {
  value = "${local.private_cidrs}"
}
output "azs" {
  value = "${local.azs}"
}
output "public_subnets_ids" {
  value = "${aws_subnet.public_subnets.*.id}"
}