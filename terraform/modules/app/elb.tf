resource "aws_elb" "demo-app-elb" {
  security_groups = ["${aws_security_group.SG-ELB.id}"]
  subnets = ["${var.public_subnets_ids}"]
  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }
  health_check {
    healthy_threshold = 2
    interval = 30
    target = "HTTP:80/"
    timeout = 5
    unhealthy_threshold = 3
  }
  tags {
    Name = "demo-app-${terraform.workspace}"
  }
}
