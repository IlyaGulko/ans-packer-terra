variable "keys" { type = "map" }
variable "vpc_id" {}
variable "vpc_cidr" {}
variable "public_cidrs" { type = "list" }
variable "private_cidrs" { type = "list" }
variable "azs" { type = "list" }
variable "public_subnets_ids" { type = "list" }
variable "aws_amis_n_types" { type = "map" }

# variable "aws_amis_n_types" {
#   type = "map"
#   default = {
#     us-east-1 = {
#       ami           = "ami-08868e3757e2aa1ee",
#       instance_type = "t2.micro"
#     }
#   }
# }
data "aws_iam_policy_document" "s3policy" {
  statement {
    actions   = ["s3:ListBucket", "s3:GetObject", "s3:PutObject"]
    resources = ["${aws_iam_role.IAMRole.arn}"]
  }
}