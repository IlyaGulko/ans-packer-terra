resource "aws_s3_bucket" "backend-configuration" {
  bucket = "backend-configuration-${terraform.workspace}"
  region = "us-east-1"
  tags {
    Name = "backend-configuration"
  }
  # provisioner "local-exec" {
  #   command = "aws s3api put-object --bucket ${aws_s3_bucket.backend-configuration.bucket} --key nginx.conf --body modules/app/nginx/nginx.conf"
  # }
}
resource "aws_s3_bucket_policy" "bucket-policy" {
  depends_on = ["aws_s3_bucket.backend-configuration"]
  bucket     = "${aws_s3_bucket.backend-configuration.id}"
  policy     = <<POLICY
{
  "Version": "2012-10-17",
  "Id": "bucket-policy",
  "Statement": [
    {
      "Sid": "IPAllow",
      "Effect": "Allow",
      "Principal": {"AWS": "${aws_iam_role.IAMRole.arn}"},
      "Action": [
                "s3:PutObject",
                "s3:GetObject"
              ],
      "Resource": "${aws_s3_bucket.backend-configuration.arn}/*"
    }
  ]
}
POLICY
}

# resource "aws_s3_bucket_object" "index-html" {
#   depends_on = ["aws_s3_bucket.backend-configuration"]
#   bucket     = "${aws_s3_bucket.backend-configuration.bucket}"
#   key        = "index.html"
#   source     = "modules/app/nginx/index.html"
# }