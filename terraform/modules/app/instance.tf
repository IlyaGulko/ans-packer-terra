resource "aws_launch_configuration" "ASGLaunchConfiguration" {
  image_id             = "${lookup(var.aws_amis_n_types["us-east-1"], "ami")}"
  instance_type        = "${lookup(var.aws_amis_n_types["us-east-1"], "instance_type")}"
  security_groups      = ["${aws_security_group.SG-EC2.id}"]
  name                 = "demo-app-${terraform.workspace}"
}
resource "aws_autoscaling_group" "ASG" {
  # count = "${terraform.workspace == "dev" ? 1 : 0}"
  depends_on           = ["aws_launch_configuration.ASGLaunchConfiguration"]
  name                 = "demo-app-01-asg-${terraform.workspace}"
  vpc_zone_identifier  = ["${var.public_subnets_ids[0]}"]
  max_size             = "${terraform.workspace == "dev" ? 1 : 2}"
  min_size             = "${terraform.workspace == "dev" ? 1 : 2}"
  launch_configuration = "${aws_launch_configuration.ASGLaunchConfiguration.id}"
}
resource "aws_autoscaling_attachment" "asg_attachment" {
  depends_on             = ["aws_autoscaling_group.ASG", "aws_elb.demo-app-elb"]
  autoscaling_group_name = "${aws_autoscaling_group.ASG.id}"
  elb                    = "${aws_elb.demo-app-elb.id}"
}