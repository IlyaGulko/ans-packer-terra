#!/usr/bin/env node

const express = require('express')
const app = express()
const port = 3000

app.get('/backend/', (req, res) => res.send('Hello World from NodeJS!'))
app.get('*', (req, res) => res.status(404).send('404 from NodeJS'))

app.listen(port, () => console.log(`Example app listening on port ${port}!`))